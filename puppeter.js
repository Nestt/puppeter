const puppeteer = require('puppeteer');

class Puppet {

	constructor() {
		this.browserWSEndpoint = Puppet.launch();
		this.browser = null;
	}

	static async launch() {
		const browser = await puppeteer.launch({headless: true});
		const browserWSEndpoint = await browser.wsEndpoint();
		browser.disconnect();
		return browserWSEndpoint;
	}

	async connect() {
		const browserWSEndpoint = await this.browserWSEndpoint;
		console.log(browserWSEndpoint);
		return this.browser = await puppeteer.connect({browserWSEndpoint});
	}

	async createPdf(url, path = null) {
		try {
			const page = await this.browser.newPage();
			// wait until 500 ms and no more than 2 active browser
			await page.goto(url, {waitUntil: 'networkidle2'});
			// Generates a PDF with 'screen' media type.
			await page.emulateMedia('screen');
			const pdfBuffer = await page.pdf({path: path, format: 'A4'});
			console.log('pdf created');
			await page.close();
			return pdfBuffer;
		} catch (e) {
			throw e;
		}
	}
}

module.exports = Puppet;
