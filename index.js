const puppeteer = require('./puppeter');

(async () => {
	const Puppet = new puppeteer();
	await Puppet.connect();
	let promise1 = Puppet.createPdf('https://www.google.com/', 'test.pdf');
	let promise2 = Puppet.createPdf('https://stackoverflow.com/', 'test2.pdf');
	await Promise.all([promise1, promise2]).catch(e => console.log(e));
	let promise3 = await Puppet.createPdf('https://www.youtube.com/', 'test3.pdf');
	console.log(promise3);
	await Puppet.browser.close();

})();
